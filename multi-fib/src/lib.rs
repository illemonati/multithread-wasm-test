mod utils;

use wasm_bindgen::prelude::*;
use rayon::prelude::*;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;


fn simple_fib(n: usize) -> usize {
    let mut a = 0;
    let mut b = 1;
    for _ in 0..n {
        let temp = b;
        b += a;
        a = temp;
    }
    a
}


#[wasm_bindgen]
pub fn multi_fib(ns: Vec<usize>) -> Vec<usize> {
    utils::set_panic_hook();
    let mut res = Vec::with_capacity(ns.len());
    for n in ns {
        res.push(simple_fib(n));
    }
    res
}


#[wasm_bindgen]
pub fn multi_fib_multithread(ns: Vec<usize>) -> Vec<usize> {
    utils::set_panic_hook();
    let mut res = Vec::with_capacity(ns.len());
    ns.par_iter().map(|n|simple_fib(*n)).collect_into_vec(&mut res);
    res
}




