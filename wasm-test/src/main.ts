

async function main() {
    const wasm = await import('multi-fib');
    const arr = new Uint32Array([0, 1, 2, 3, 4]);
    const res = wasm.multi_fib(arr);
    console.log(res);


    const res2 = wasm.multi_fib_multithread(arr);
    console.log(res2);

}

main().then();


